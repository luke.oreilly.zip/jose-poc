﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using Microsoft.IdentityModel.Tokens;

namespace CreateJwe
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Let's make a JWE...");
            
            // Recipient
            var encryptingCredentials = new X509EncryptingCredentials(
                new X509Certificate2("../../../../../certs/certificateRecipient.pem"));

            // Sender (this app)
            var senderPrivateKeyText = File.ReadAllText("../../../../../certs/privateKeySender.pem");
            var senderPrivateKey = RSA.Create();
            senderPrivateKey.ImportFromPem(senderPrivateKeyText);
            var signingCredentials = new SigningCredentials(
                new RsaSecurityKey(senderPrivateKey), SecurityAlgorithms.RsaSha256);

            var handler = new JwtSecurityTokenHandler();
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Audience = "you",
                Issuer = "me",
                Subject = new ClaimsIdentity(new List<Claim> {new Claim("sub", "Luke")}),
                SigningCredentials = signingCredentials,
                EncryptingCredentials = encryptingCredentials
            };

            var token = handler.CreateEncodedJwt(tokenDescriptor);
            
            Console.WriteLine(token);
        }
    }
}