﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Microsoft.IdentityModel.Tokens;

namespace ReadJwe
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Let's read a JWE...");
            
            var token = "eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGVuYyNyc2Etb2FlcCIsImVuYyI6IkExMjhDQkMtSFMyNTYiLCJraWQiOiJGRjYyQUJDNEQwMkY0MDU1RERBNEMzQzg0QzMwRkQ5MkM2OEUxQjU5IiwidHlwIjoiSldUIn0.IVzk4p9EMZ6Dxl5pGv1qgKvGfQiQE07d4yYchKKoe-7HWQN4oog8u0zogtHtj_syt1DoGTBJhKIP3tudeTzvCtiRV1HkwCPaGVl-BwxqD_kHuO04vn1j-1FYop9mUV6tOV_EvNOOtabIdjaL-Vq6fFNp9NIw_zVzmtuVwP-70hV9s5S-RohqAVpm9CgGXG6zdkr8aY7cDJmLXQOpvNJdo50qSMgbEBvK9GGBcdrKltZzcv-Kk7c-2fgaxAST4JudyPqaNTUm-kXFF6el8MD7l20t1eQwXXSqC0vbdMgQD1X12hOLuM3lKLF5Gbyvv2aEAx5xE77ya6DbTgBJgbZpM8K04MmeoEFBAu_X7ncVAKH3IiGtWla42lCJtE_S7ZLY1kEkwMwQIsGbJgt3FuqBZLrLhtAAwGEPG_jTp5YKjyVZJiTmHnADJYmZ9aSumHkoGCsDsQwVoPRoFf71ySvy_5NPEKPTX6dawIILG1H2Qj5kqT7dZYPXAipl5A9gHLwlJw47KVzvYwtWCEEK0LMKtV-6_VYnfpdwqD0R2T9mKeBzv_a12nVCk0dhokFSTd81VZqPXPQD8ZsOK8XNen98JUX4n1Y_AuNPFTeSrUpBMJ-Z1BRiTW7PkyDMHzDBdSpCnOvkvXwVlTkZNGl67l6DgwrFTqfUZOuSCjq9R5mvtxs.ks7WQCCwbj8cldFU_F_vag.Wmtjek6zOTZHqOpcd5pp52fUQM3a247BFlZZT1dgkWrqpuGtybQnOMjkOPfm7I15NBQdKhHiAQN9x6_MfXapHkw1W8rNK3E8-J9XEMuA7ygGW2NcnP-YROHD5BRY7p-UCviDyRh1ziNvSoj5suK7FSrewULbJvBYkiXJgfDp-IKnS19TrC3kS6-4WvtfN4ljgM_Eb_pS7s13-ja7F5uNe6NFsdZIszqRO_MlVoi8NnXxx-1QmmshPhPPWfzCw17Zxr5TDCWJ8kKKeLOauj1pf0unCy84A4sGnG7q_8kYOoobuhDHfujexNBUck90BLsfVvIvzaGmdhesE_92MqLxFyS6FBEg1Tr7eEt2ac1REkMXYL042x8EJytu-HLSlZjv4s6wIwynxC8tDHFh_D6fmDWVKgMzzTlXw0YbNTuJe7sJLUMWk9Y0MbGC5_yrKdSohgolvrbPjMGlLD1tXhOGlDZSJAjBbwr5VSzC7jh_g4-HhzAlwjSuhxT73fhmSwDQA9VRJ7U4STpEpx-YHx98wMud7WcW2P4NuiAhn_7gXKVYOdFsQ1qAztwrW7vk-THN5IDLA3Hry0c5glA8bN-_bVZKHnWddAUyYtGGoTGIi3aBJnBrOAE3C-UpW8PMyV4RGJrzHtUfyn7TuvpIUK1gtDXPmJEeFVvuFkNxiKarwVPcnfNbm5kAghZ4Gw4pmmEE41XQY-d6Zw8ido_LNmN3CFcHQ6uk9BkR1ZYZBHnsAVMroY5SYos9NCfg-4E3lkHzYtfmyp3IFYb7tI1Jo56cbOcf_Hku0FRzw0R6M7Od6lNmeeGS-ix0dGmjWlLERj4sQ9_HQPRpN0AIBZb2vV9F6fwwobjWCB1njfzSbeLOIEaYSy63JzsEdstQEGTVRsnrEYX-v1-Aqg9RbV7vH1sfQeES5zAsov1bwMbHOsE5kVklnB5i65EUgIMweCD71gHjakfYOIbctwuxqLeZjoktvYDTuhIPyMftq2zj6NcduTf2TbZ7QusJ2q5stcqofdxlO5Ooz3awj_Cl9i1Mg-RNXCKg1kqi81EjmYlGnZt5BKgG91ROPl3L-mvAK1WfEwRaCMNPYAl1Z0l3KUM_cQAruMSHDc6PAz2l---XQouTva8.cuNOc9qZGa7ywRTFD8KPBw";
            
            // Recipient (this app)
            var keyPem = File.ReadAllText("../../../../../certs/privateKeyRecipient.pem");
            var certPem = File.ReadAllText("../../../../../certs/certificateRecipient.pem");
            var recipientDecryptionKey = new X509SecurityKey(X509Certificate2.CreateFromPem(certPem, keyPem));
            
            // Sender
            var senderCert = new X509Certificate2("../../../../../certs/certificateSender.pem");
            var senderSignatureVerificationKey = new X509SecurityKey(senderCert);

            var handler = new JwtSecurityTokenHandler();
            var claimsPrincipal = handler.ValidateToken(
                token,
                new TokenValidationParameters
                {
                    ValidAudience = "you",
                    ValidIssuer = "me",
                    RequireSignedTokens = false,
                    TokenDecryptionKey = recipientDecryptionKey,
                    IssuerSigningKey = senderSignatureVerificationKey
                },
                out SecurityToken securityToken);
            
            Console.WriteLine(securityToken);
        }
    }
}