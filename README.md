# Notes

### Generate self signed x509 certificates
`openssl req -x509 -newkey rsa:4096 -keyout privateKeySender.pem -out certificateSender.pem -days 365 -nodes`

`openssl req -x509 -newkey rsa:4096 -keyout privateKeyRecipient.pem -out certificateRecipient.pem -days 365 -nodes`

### Inspect x509 certificates
`openssl x509 -in certificateSender.pem -noout -text`

`openssl x509 -in certificateRecipient.pem -noout -text`
